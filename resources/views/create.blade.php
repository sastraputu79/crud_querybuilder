@extends('layout.main')

@section('content')
            <div class="row">
                <div class="col-9">
                    <h1>Tambah Data</h1>
                </div>
            <br/>
            
            <form action="/input" method="POST">
                {{ csrf_field() }}
                <div class="mb-3">
                    <label class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama_mahasiswa" required="required">
                </div>
                <div class="mb-3">
                    <label class="form-label">NIM</label>
                    <input type="text" class="form-control" name="nim_mahasiswa" required="required">
                </div>
                <div class="mb-3">
                    <label class="form-label">Kelas</label>
                    <input type="text" class="form-control" name="kelas_mahasiswa" required="required">
                </div>
                <div class="mb-3">
                    <label class="form-label">Program Studi</label>
                    <input type="text" class="form-control" name="prodi_mahasiswa" required="required">
                </div>
                <div class="mb-3">
                    <label class="form-label">Fakultas</label>
                    <input type="text" class="form-control" name="fakultas_mahasiswa" required="required">
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
                <br>
            </div>
@endsection