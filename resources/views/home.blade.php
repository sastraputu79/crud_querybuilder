@extends('layout.main')

@section('content')

            @if(session('berhasil'))
            <div class="alert alert-success mt-2" role="alert">
            {{ session('berhasil') }}
            </div>
            @endif
            <div class="row">
                <div class="col-9">
                    <h1>Data Mahasiswa</h1>
                </div>
                <div class="col-3">
                    <a href="/tambah" class="btn btn-primary mt-2 pull-right">Tambah Data</a>
                </div>
                <table class="table table-hover table-bordered border-primary table-striped">
                    <thead class="table-balck">
                        <tr>
                            <th>Nama</th>
                            <th>NIM</th>
                            <th>Kelas</th>
                            <th>Prodi</th>
                            <th>Fakultas</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    @foreach($mahasiswa as $home)
                    <tr>
                        <td>{{ $home->nama_mahasiswa }}</td>
                        <td>{{ $home->nim_mahasiswa }}</td>
                        <td>{{ $home->kelas_mahasiswa }}</td>
                        <td>{{ $home->prodi_mahasiswa }}</td>
                        <td>{{ $home->fakultas_mahasiswa }}</td>
                        <td>
                            <a href="/edit/{{ $home->id }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                            <a href="/hapus/{{ $home->id }}" class="btn btn-danger" onclick="return confirm('Apakah ingin menghapus data?')"><i class="fa fa-trash"></i> Hapus</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
@endsection