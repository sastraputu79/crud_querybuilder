@extends('layout.main')

@section('content')
            <div class="row">
                <div class="col-9">
                    <h1>Update Data</h1>
                </div>
            <br/>
            
            @foreach($mahasiswa as $update)
            <form action="/update" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $update->id }}"> 
                <br/>
                <div class="mb-3">
                    <label class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama_mahasiswa" required="required" value="{{ $update->nama_mahasiswa }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">NIM</label>
                    <input type="text" class="form-control" name="nim_mahasiswa" required="required" value="{{ $update->nim_mahasiswa }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Kelas</label>
                    <input type="text" class="form-control" name="kelas_mahasiswa" required="required" value="{{ $update->kelas_mahasiswa }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Program Studi</label>
                    <input type="text" class="form-control" name="prodi_mahasiswa" required="required" value="{{ $update->prodi_mahasiswa }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Fakultas</label>
                    <input type="text" class="form-control" name="fakultas_mahasiswa" required="required" value="{{ $update->fakultas_mahasiswa }}">
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
            @endforeach
                <br>
            </div>
@endsection