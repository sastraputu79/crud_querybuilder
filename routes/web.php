<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[DataController::class,'home']);
Route::get('/tambah',[DataController::class,'tambah']);
Route::post('/input',[DataController::class,'input']);
Route::get('/edit/{id}',[DataController::class,'edit']);
Route::post('/update',[DataController::class,'update']);
Route::get('/hapus/{id}',[DataController::class,'hapus']);