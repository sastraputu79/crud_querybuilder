<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataController extends Controller
{
    public function home()
    {
        $datamahasiswa = DB::table('mahasiswa')->get();
        return view('home',['mahasiswa' => $datamahasiswa]);
    }

    public function tambah()
    {
        return view('create');
    }

    public function input(Request $request)
    {
        
        DB::table('mahasiswa')->insert([
            'nama_mahasiswa' => $request->nama_mahasiswa,
            'nim_mahasiswa' => $request->nim_mahasiswa,
            'kelas_mahasiswa' => $request->kelas_mahasiswa,
            'prodi_mahasiswa' => $request->prodi_mahasiswa,
            'fakultas_mahasiswa' => $request->fakultas_mahasiswa]
        );

        return redirect('/')->with('berhasil','Data berhasil diinput!');
    }

    public function edit($id)
    { 
        $datamahasiswa = DB::table('mahasiswa')->where('id',$id)->get(); 
        return view('edit',['mahasiswa' => $datamahasiswa]);
    }

    public function update(Request $request)
    {
        DB::table('mahasiswa')->where('id',$request->id)->update([

            'nama_mahasiswa' => $request->nama_mahasiswa,
            'nim_mahasiswa' => $request->nim_mahasiswa,
            'kelas_mahasiswa' => $request->kelas_mahasiswa,
            'prodi_mahasiswa' => $request->prodi_mahasiswa,
            'fakultas_mahasiswa' => $request->fakultas_mahasiswa
        ]);

        return redirect('/')->with('berhasil','Data berhasil diupdate!');
    }

    public function hapus($id)
    {
        DB::table('mahasiswa')->where('id',$id)->delete();
        return redirect('/')->with('berhasil','Data berhasil dihapus!');
    }
}

